/**
 * Created by pohchye on 11/7/2016.
 */
// Create the Express app
var express=require("express");
var app=express();

// Require this for POST method
var bodyParser=require("body-parser");
app.use(bodyParser.urlencoded({'extended': 'true'}));
app.use(bodyParser.json());

// Middleware

// - Register User
app.post("/register", function (req, res) {
    // Retrieve task
    var user = JSON.parse(req.body.params.user);
    // console.info("Type for user: %s", typeof user);
    console.info("> User info from client: ");
    // Display user send from client
    for (var key in user) {
        if (user.hasOwnProperty(key)) {
            var value = user[key];
            console.info("%s = %s", key, value);
        }
    }
    // Validate the Age
    var today_date = new Date();
    var age_valid = "false";
    var user_dob = Date.parse(user.dob);
    console.info("Today Date: %s , User DOB", today_date.getTime(), user_dob);
    var age = (today_date.getTime()-user_dob);
    if (age >= 567993600000) {
        // 567993600000 is the epoch time on Jan 1 1988
        age_valid = "true";
        // console.info("User " + user.name + " age is valid");
        console.info("Registration for user " + user.name + " is successful");
        res.status(202).end();
    } else {
        console.info("Registration for User " + user.name + " is unsuccessful: Age is invalid");
        res.status(422).end();
    }
});

// Response to user on status
app.get("/thankyou", function (req, res) {
    res.redirect("thankyou.html");
});

// Serve public files
app.use(express.static(__dirname + "/public"));
// Handle Errors
app.use(function (req, res, http) {
    res.redirect("error.html");
});

// Set port
app.set("port",
    process.argv[2] || process.env.APP_PORT || 3000
);

// Start the Express server
app.listen(app.get("port"), function () {
    console.info("Express server started on port %s", app.get("port"));
});