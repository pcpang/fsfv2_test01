/**
 * Created by pohchye on 11/7/2016.
 */
var UserRegApp = angular.module("UserRegApp", []);

(function () {
    var UserRegCtrl;
    UserRegCtrl = function ($http, $window) {
        var ctrl = this;
        // ctrl.error = 0;
        ctrl.newUser = {
            name: "",
            password: "",
            email: "",
            gender: "",
            dob: "",
            address: "",
            country: "",
            contact: ""
        };
        ctrl.status = {
            message: "",
            code: 0
        };

        // Register User
        ctrl.register = function () {
            // Display user input from view
            console.info("> New User Info");
            console.info("Name: %s", ctrl.newUser.name);
            console.info("Password: %s", ctrl.newUser.password);
            console.info("Email: %s", ctrl.newUser.email);
            console.info("Gender: %s", ctrl.newUser.gender);
            console.info("Date of Birth: %s", ctrl.newUser.dob);
            console.info("Address: %s", ctrl.newUser.address);
            console.info("Country: %s", ctrl.newUser.country);
            console.info("Contact: %s", ctrl.newUser.contact);
            $http.post("/register", {
                params: {
                    user: JSON.stringify(ctrl.newUser),
                    headers: {'Content-Type': 'application/json'}
                }
            }).then( function () {
                console.info("Success in adding new user");
                ctrl.status.message="Your registration is successful";
                ctrl.status.code= 200;
                ctrl.newUser = {
                    name: "",
                    password: "",
                    email: "",
                    gender: "",
                    dob: "",
                    address: "",
                    country: "",
                    contact: ""
                };
                // ctrl.status = {
                //     message: "",
                //     code: 0
                // };
                $window.location = "/thankyou";
            }).catch( function () {
                console.info("Failure in adding new user: Age is below 18");
                ctrl.status.message="Your registration failed, age is below 18";
                ctrl.status.code= 400;
                // $window.location = "/error";
            });
        };
    };

    UserRegApp.controller("UserRegCtrl", ["$http", "$window", UserRegCtrl]);
})();